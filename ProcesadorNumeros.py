class Procesador:
    def procesar(self, cadena):
        if cadena == '':
            return [0,0,0,0]
        elif ',' in cadena:
            numeros = cadena.split(',')
            numeros = [int(i) for i in numeros]
            print (sum(numeros)/len(numeros))
            return [len(numeros),min(numeros),max(numeros),(sum(numeros)/len(numeros))]
        else:
            return [1,int(cadena),int(cadena),int(cadena)]