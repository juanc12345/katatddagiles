from unittest import TestCase

from ProcesadorNumeros import Procesador

class ProcesadorTest(TestCase):
    def test_procesarNumeroElementosCadenaVacia(self):
        resultado = Procesador().procesar('')
        self.assertEqual(resultado[0],0,'Cadena vacia numero elementos')

    def test_procesarNumeroElementosUnNumero(self):
        resultado = Procesador().procesar('5')
        self.assertEqual(resultado[0],1,'Un numero, numero de elementos')

    def test_procesarNumeroElementosDosNumeros(self):
        resultado = Procesador().procesar('5,6')
        self.assertEqual(resultado[0],2,'Dos numeros, numero de elementos')

    def test_procesarNumeroElementosNNumeros(self):
        resultado = Procesador().procesar('5,6,4,2,3,5,76,343,0,1')
        self.assertEqual(resultado[0],10,'N numeros, numero de elementos')

    def test_procesarNumeroElementosMinimoCadenaVacia(self):
        resultado = Procesador().procesar('')
        self.assertEqual(resultado[0],0,'Cadena vacia numero elementos y minimo')
        self.assertEqual(resultado[1],0,'Cadena vacia numero elementos y minimo')

    def test_procesarNumeroElementosMinimoUnNumero(self):
        resultado = Procesador().procesar('3')
        self.assertEqual(resultado[0],1,'Un numero, numero elementos y minimo')
        self.assertEqual(resultado[1],3,'Un numero, numero elementos y minimo')

    def test_procesarNumeroElementosMinimoDosNumeros(self):
        resultado = Procesador().procesar('4,5')
        self.assertEqual(resultado[0],2,'Dos numeros, numero elementos y minimo')
        self.assertEqual(resultado[1],4,'Dos numeros, numero elementos y minimo')

    def test_procesarNumeroElementosMinimoNNumeros(self):
        resultado = Procesador().procesar('3,7,12,3,45,6,87')
        self.assertEqual(resultado[0],7,'N numeros, numero elementos y minimo')
        self.assertEqual(resultado[1],3,'N numeros, numero elementos y minimo')

    def test_procesarNumeroElementosMinimoMaxCadenaVacia(self):
        resultado = Procesador().procesar('')
        self.assertEqual(resultado[0],0,'Cadena vacia numero elementos y minimo')
        self.assertEqual(resultado[1],0,'Cadena vacia numero elementos y minimo')
        self.assertEqual(resultado[2],0,'Cadena vacia numero elementos y minimo')

    def test_procesarNumeroElementosMinimoMaxUnNumero(self):
        resultado = Procesador().procesar('3')
        self.assertEqual(resultado[0],1,'Un numero, numero elementos y minimo')
        self.assertEqual(resultado[1],3,'Un numero, numero elementos y minimo')
        self.assertEqual(resultado[2],3,'Un numero, numero elementos y minimo')

    def test_procesarNumeroElementosMinimoMaxDosNumeros(self):
        resultado = Procesador().procesar('4,5')
        self.assertEqual(resultado[0],2,'Dos numeros, numero elementos y minimo')
        self.assertEqual(resultado[1],4,'Dos numeros, numero elementos y minimo')
        self.assertEqual(resultado[2],5,'Dos numeros, numero elementos y minimo')

    def test_procesarNumeroElementosMinimoMaxNNumeros(self):
        resultado = Procesador().procesar('3,7,12,3,45,6,87')
        self.assertEqual(resultado[0],7,'N numeros, numero elementos y minimo')
        self.assertEqual(resultado[1],3,'N numeros, numero elementos y minimo')
        self.assertEqual(resultado[2],87,'N numeros, numero elementos y minimo')

    def test_procesarNumeroElementosMinimoMaxPromCadenaVacia(self):
        resultado = Procesador().procesar('')
        self.assertEqual(resultado[0],0,'Cadena vacia numero elementos, min, max, prom')
        self.assertEqual(resultado[1],0,'Cadena vacia numero elementos, min, max, prom')
        self.assertEqual(resultado[2],0,'Cadena vacia numero elementos, min, max, prom')
        self.assertEqual(resultado[3],0,'Cadena vacia numero elementos, min, max, prom')

    def test_procesarNumeroElementosMinimoMaxPromUnNumero(self):
        resultado = Procesador().procesar('9')
        self.assertEqual(resultado[0],1,'Un numero: numero elementos, min, max, prom')
        self.assertEqual(resultado[1],9,'Un numero: numero elementos, min, max, prom')
        self.assertEqual(resultado[2],9,'Un numero: numero elementos, min, max, prom')
        self.assertEqual(resultado[3],9,'Un numero: numero elementos, min, max, prom')

    def test_procesarNumeroElementosMinimoMaxPromDosNumeros(self):
        resultado = Procesador().procesar('9,1')
        self.assertEqual(resultado[0],2,'Dos numero: numero elementos, min, max, prom')
        self.assertEqual(resultado[1],1,'Dos numero: numero elementos, min, max, prom')
        self.assertEqual(resultado[2],9,'Dos numero: numero elementos, min, max, prom')
        self.assertEqual(resultado[3],5,'Dos numero: numero elementos, min, max, prom')

    def test_procesarNumeroElementosMinimoMaxPromNNumeros(self):
        resultado = Procesador().procesar('9,1,1,2,3,78,5,4,0')
        self.assertEqual(resultado[0],9,'N numero: numero elementos, min, max, prom')
        self.assertEqual(resultado[1],0,'N numero: numero elementos, min, max, prom')
        self.assertEqual(resultado[2],78,'N numero: numero elementos, min, max, prom')
        self.assertEqual(resultado[3],11,'N numero: numero elementos, min, max, prom')